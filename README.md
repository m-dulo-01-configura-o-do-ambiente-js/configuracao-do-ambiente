# Desafio 01 - Configuração do ambiente

🎊 Sua única limitação é você mesmo! 🎊 

---

💪****Sobre o Desafio**** 💪

Seu objetivo é Instalar e configurar o Visual Studio Code utilizando tudo o que aprendeu na aula de configuração do ambiente.

---

- Baixar e Instalar o [**Visual Studio Code**](https://code.visualstudio.com/)
- Instalar as Extensões: *Dracula Official, EditorConfig for VS Code, Prettier code formatter e Material Icon Theme*
- Aplicar os temas: *Dracula Official e Material Icon theme*
- Instalar a fonte [**Fira Code**](https://github.com/tonsky/FiraCode).
- Adicionar configurações de formatação e fonte modificando o arquivo *settings.json* do Visual Studio Code.

---

💪 **Arquivo settings.json:**

```json
{
  "workbench.colorTheme": "Dracula",
  "workbench.iconTheme": "vs-nomo-dark",
  
  "editor.fontSize": 18,
  "editor.formatOnSave": true,
  "editor.fontFamily": "Fira Code Regular, Consolas, 'Courier New', monospace",
  "editor.fontLigatures": true,

  "prettier.enable": true,

  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },

  "prettier.singleQuote": true,
  "prettier.useEditorConfig": false,
  "prettier.printWidth": 80,

  "redhat.telemetry.enabled": false,
  "yaml.format.singleQuote": true,

  "terminal.integrated.fontFamily": "Consolas",
  
  "[typescript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnSave": true
  }
}
```
